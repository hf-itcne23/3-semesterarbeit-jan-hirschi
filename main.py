from app import create_app
from gunicorn.app.base import BaseApplication

# Define a custom Gunicorn application class to run the Flask application
class StandaloneApplication(BaseApplication):
    # Initialize the custom Gunicorn application
    def __init__(self, app, options=None):
        self.application = app  # Store the Flask application instance
        self.options = options or {}  # Store the options or an empty dict if none provided
        super().__init__()

    # Load configuration settings into Gunicorn
    def load_config(self):
        # Only set values that are specified in the options and are not None
        config = {
            key: value
            for key, value in self.options.items()
            if key in self.cfg.settings and value is not None
        }
        for key, value in config.items():
            self.cfg.set(key.lower(), value)  # Set the Gunicorn configuration

    # Return the Flask application instance
    def load(self):
        return self.application

# Import the application factory function
app = create_app()

# Main execution block
if __name__ == "__main__":
    # Define Gunicorn options
    options = {
        "bind": "%s:%s" % ("0.0.0.0", "8080"),  # Bind to all interfaces on port 8080
        "workers": 4,  # Use 4 worker processes
        "loglevel": "info",  # Set the logging level to info
        "accesslog": "-"  # Log access requests to stdout
    }
    # Create and run the custom Gunicorn application
    StandaloneApplication(app, options).run()
