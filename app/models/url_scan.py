from apiflask import Schema
from apiflask.fields import String

# Define a schema for URL input using APIFLASK
class UrlIn(Schema):
    url = String()
