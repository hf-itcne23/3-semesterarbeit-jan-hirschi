from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

# Use relative import to import User
from .users import User

__all__ = ['db', 'User']
