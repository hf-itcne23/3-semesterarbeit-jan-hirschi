from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import db  # Use relative import for db

class User(db.Model, UserMixin):
    __tablename__ = 'user'
    __table_args__ = {'extend_existing': True}  # Added to extend existing table
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=True)
    password_hash = db.Column(db.String(128))
    is_active = db.Column(db.Boolean, default=True, nullable=False)

    # Set the user's password, hashing it for storage
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    # Check the user's password against the stored hash
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
