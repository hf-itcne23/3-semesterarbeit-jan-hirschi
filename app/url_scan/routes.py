from flask import request, jsonify
from . import bp
from app.models.url_scan import UrlIn
import requests
from urllib.parse import urlparse
import time

@bp.route('/', methods=['POST'])
@bp.input(UrlIn)
def scan_url(json_data):
    url_to_scan = json_data.get('url')
    
    # Check if the URL has a scheme. If not, default to HTTPS
    parsed_url = urlparse(url_to_scan)
    if not parsed_url.scheme:
        url_to_scan = "https://" + url_to_scan  # Default to HTTPS if no scheme is present

    # Perform the URL check logic, for example with requests
    response = requests.get(url_to_scan, timeout=10)

    print(url_to_scan)

    # Set up the VirusTotal API URL and API key
    url = "https://www.virustotal.com/api/v3/urls"
    apikey = "a17eebf0fd839d2795ed08e7a57b6c80de45a6a89b31eceddc7647220b41e692"
    
    # Prepare the payload and headers for the request to VirusTotal
    payload = { "url": url_to_scan }
    headers = {
        "accept": "application/json",
        "x-apikey": apikey,
        "content-type": "application/x-www-form-urlencoded"
    }
    
    # Send the URL to VirusTotal for scanning
    response = requests.post(url, data=payload, headers=headers)
    
    # Extract the link to check the scan results
    url2 = response.json()["data"]["links"]["self"]
    
    headers = {
        "accept": "application/json",
        "x-apikey": apikey
    }

    # Loop to check the status of the analysis
    while True:
        response = requests.get(url2, headers=headers, timeout=10)
        response_json = response.json()

        status = response_json["data"]["attributes"]["status"]
        if status == "completed":
            if response_json["data"]["attributes"]["stats"]["malicious"] > 0:
                return jsonify({"result": "Malicious content found"})
            elif response_json["data"]["attributes"]["stats"]["suspicious"] > 0:
                return jsonify({"result": "Suspicious content found"})
            else:
                return jsonify({"result": "No malicious or suspicious content found"})
        elif status == "queued" or status == "in-progress":
            print("Analysis is still in progress, waiting for completion...")
            time.sleep(0.5)  # Wait for 0.5 seconds before checking again
        else:
            raise ValueError("Unexpected status value: {}".format(status))
