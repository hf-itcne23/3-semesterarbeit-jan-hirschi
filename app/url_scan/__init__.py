# url_scan/__init__.py
from apiflask import APIBlueprint

# Create an APIBlueprint named 'url_scan'
bp = APIBlueprint('url_scan', __name__)

# Import routes using a relative import, this must occur after the definition of bp
from app.url_scan import routes
