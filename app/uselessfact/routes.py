from flask import request, jsonify
from . import bp
import requests

@bp.route('/', methods=['GET'])
def get_useless_fact():
    # URL to fetch a random useless fact
    url = 'https://uselessfacts.jsph.pl/random.json?language=en'
    
    # Make a GET request to the URL
    response = requests.get(url, timeout=10)
    
    # Check if the response is successful
    if response.status_code == 200:
        fact_data = response.json()  # Parse the JSON response
        fact = fact_data.get("text")  # Extract the fact from the response
        return jsonify({"fact": fact})  # Return the fact as JSON
    else:
        # Return an error message if the request failed
        return jsonify({"error": "Failed to fetch fact"}), 500
