from apiflask import APIBlueprint

# Create an APIBlueprint named 'uselessfact'
bp = APIBlueprint('uselessfact', __name__)

# Import routes using a relative import, this must occur after the definition of bp
from app.uselessfact import routes
