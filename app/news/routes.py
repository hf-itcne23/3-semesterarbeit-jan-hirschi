from flask import request, jsonify
from . import bp
import requests

@bp.route('/', methods=['GET'])
def get_security_news():
    # API key for the news service
    api_key = '9e0ce5f9adee4c6797f22ac1417e3889'
    # Query parameter for the news search
    query = 'cybersecurity'
    # Construct the URL for the news API request
    url = f'https://newsapi.org/v2/everything?q={query}&sortBy=publishedAt&apiKey={api_key}'
    
    # Make a GET request to the news API with a timeout
    response = requests.get(url, timeout=10)
    
    # Check if the response is successful
    if response.status_code == 200:
        news_data = response.json()  # Parse the JSON response
        articles = news_data.get('articles', [])  # Extract the articles
        return jsonify({"articles": articles})  # Return the articles as JSON
    else:
        # Return an error message if the request failed
        return jsonify({"error": "Failed to fetch news"}), 500
