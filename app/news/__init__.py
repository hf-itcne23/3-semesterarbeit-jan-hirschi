from apiflask import APIBlueprint

# Create an APIBlueprint named 'news'
bp = APIBlueprint('news', __name__)

# Import routes using a relative import
from app.news import routes
