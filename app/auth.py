# auth.py

from flask import Blueprint, render_template, redirect, request, url_for, flash, current_app
from flask_login import login_user, logout_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
from app.models import db
from app.models.users import User
import logging
import os

# Create a Blueprint for authentication routes
auth = Blueprint('auth', __name__)
logger = logging.getLogger('auth')

@auth.route('/login', methods=['GET', 'POST'])
def login():
    # Handle login form submission
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        logger.info(f"Received login request for username: {username}")

        # Query the database for the user
        user = User.query.filter_by(username=username).first()
        if user:
            logger.info(f"User {username} found in database.")
            # Check the password
            if check_password_hash(user.password_hash, password):
                # Log in the user
                login_user(user)
                logger.info(f"User {username} logged in successfully.")
                return redirect(url_for('home_route'))
            else:
                logger.warning(f"User {username} provided an incorrect password.")
                flash('Incorrect password, try again.', category='error')
        else:
            logger.warning(f"Username {username} does not exist.")
            flash('Username does not exist.', category='error')

    # Render the login template
    return render_template('login.html')

@auth.route('/logout')
@login_required
def logout():
    # Log out the current user
    logout_user()
    return redirect(url_for('home_route'))

@auth.route('/register', methods=['GET', 'POST'])
def register():
    # Handle registration form submission
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        password1 = request.form.get('password1')
        invitation_token = request.form.get('invitation_token')

        logger.info(f"Received registration request for username: {username}, email: {email}, token: {invitation_token}")
        logger.info(f"Expected invitation token: {current_app.config['VALID_INVITATION_TOKEN']}")

        # Validate the form inputs
        if not username or not password1 or not invitation_token:
            flash('Username, password, and invitation token are required.', category='error')
            return redirect(url_for('auth.register'))

        # Check if the invitation token is valid
        if invitation_token != current_app.config['VALID_INVITATION_TOKEN']:
            flash('Invalid invitation token.', category='error')
            return redirect(url_for('auth.register'))

        # Check if the username or email already exists
        user_by_email = User.query.filter_by(email=email).first() if email else None
        user_by_username = User.query.filter_by(username=username).first()
        if user_by_email or user_by_username:
            flash('Email or username already exists.', category='error')
        elif len(username) < 4:
            flash('Username must be greater than 3 characters.', category='error')
        elif email and len(email) < 4:
            flash('Email must be greater than 3 characters.', category='error')
        elif len(password1) < 1:
            flash('Password must be at least 1 character.', category='error')
        else:
            # Generate a password hash
            password_hash = generate_password_hash(password1)
            logger.info(f"Registering user {username} with password hash: {password_hash}")
            # Create a new user and save to the database
            new_user = User(username=username, email=email if email else None, password_hash=password_hash)
            db.session.add(new_user)
            db.session.commit()
            # Log in the new user
            login_user(new_user)
            logger.info(f"User {username} registered and logged in successfully.")
            return redirect(url_for('home_route'))

    # Render the registration template
    return render_template('register.html')
