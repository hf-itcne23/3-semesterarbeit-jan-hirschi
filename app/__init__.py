# app/__init__.py

import os
import logging
from flask import Flask, render_template, request, url_for
import requests
from flask_login import LoginManager, login_required
from apiflask import APIFlask
from sqlalchemy import create_engine, inspect, text
from .models import db
from .models.users import User
from .auth import auth as auth_blueprint
from .url_scan import bp as url_scan_bp
from .uselessfact import bp as uselessfact_bp
from .news import bp as news_bp

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Initialize the login manager
login_manager = LoginManager()
login_manager.login_view = 'auth.login'

# Function to create the database if it does not exist
def create_database_if_not_exists(database_url):
    engine_url = database_url.rsplit('/', 1)[0] + '/'
    database_name = database_url.rsplit('/', 1)[1]

    engine = create_engine(engine_url)
    conn = engine.connect()

    conn.execute(text(f"CREATE DATABASE IF NOT EXISTS `{database_name}`"))
    conn.close()

# Function to create the tables if they do not exist
def create_tables_if_not_exists(app):
    with app.app_context():
        inspector = inspect(db.engine)
        tables = inspector.get_table_names()
        if 'user' not in tables:
            db.create_all()

# Application factory function
def create_app(config_class='config.Config'):
    app = APIFlask(__name__)
    app.config.from_object(config_class)

    # Ensure the VALID_INVITATION_TOKEN is read correctly
    app.config['VALID_INVITATION_TOKEN'] = os.environ.get('INVITATION_CODE', 'default_token')

    # Create the database if not using SQLite
    if not app.config['SQLALCHEMY_DATABASE_URI'].startswith("sqlite"):
        create_database_if_not_exists(app.config['SQLALCHEMY_DATABASE_URI'])

    # Initialize extensions
    db.init_app(app)
    login_manager.init_app(app)

    # Create tables if they do not exist
    create_tables_if_not_exists(app)

    # Register blueprints
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(url_scan_bp, url_prefix='/url_scan')
    app.register_blueprint(uselessfact_bp, url_prefix='/uselessfact')
    app.register_blueprint(news_bp, url_prefix='/news')

    # User loader callback for Flask-Login
    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    # Route for the home page
    @app.route("/")
    def home_route():
        return render_template("home.html")

    # Route for security news page
    @app.route("/security-news", methods=["GET", "POST"])
    @login_required
    def security_news():
        submitted_url = None
        phone_info = "Please submit a phone number for validation."
        phone_number = None
        weather_info = "Please submit a location for weather information."
        location = None
        url_scan_result = None
        useless_fact = None
        if request.method == "POST":
            submitted_url = request.form.get("url")
            phone_number = request.form.get("phone_number")
            location = request.form.get("location")
            if phone_number:
                try:
                    api_key = os.environ['PHONE_VALIDATION_API_KEY']
                    phone_response = requests.get(f"http://apilayer.net/api/validate?access_key={api_key}&number={phone_number}", timeout=10)
                    phone_data = phone_response.json()
                    logger.info(f"Phone validation response: {phone_data}")
                    if 'valid' in phone_data:
                        valid = phone_data['valid']
                        country = phone_data.get('country_name', 'Unknown')
                        line_type = phone_data.get('line_type', 'Unknown')
                        carrier = phone_data.get('carrier', 'Unknown')
                        phone_info = f"Valid: {valid}, Country: {country}, Line Type: {line_type}, Carrier: {carrier}"
                    else:
                        phone_info = f"Invalid response: {phone_data}"
                except Exception as e:
                    logger.error(f"Failed to validate phone number: {str(e)}")
                    phone_info = "Failed to validate phone number due to an error."
            if submitted_url:
                try:
                    scan_response = requests.post(url_for('url_scan.scan_url', _external=True), json={"url": submitted_url})
                    scan_result = scan_response.json()
                    url_scan_result = scan_result.get("result", "No result")
                except Exception as e:
                    logger.error(f"Failed to scan URL: {str(e)}")
                    url_scan_result = "Failed to scan URL due to an error."
        return render_template("security_news.html", 
                               submitted_url=submitted_url, 
                               phone_info=phone_info, 
                               phone_number=phone_number, 
                               weather_info=weather_info, 
                               location=location, 
                               display_phone_response=True, 
                               url_scan_result=url_scan_result, 
                               useless_fact=useless_fact)

    return app
