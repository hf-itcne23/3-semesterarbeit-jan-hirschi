import pytest
from app import create_app
import os
import sys

# Add the 'tests' directory to the Python path
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

@pytest.fixture
def client():
    # Create the application with a specific test configuration
    app = create_app('test_config.Config')
    app.config.update({
        "TESTING": True,  # Enable testing mode
        "SESSION_COOKIE_NAME": "session",  # Use the default session cookie name or customize it
        "SESSION_TYPE": "filesystem",  # Ensure the session type is set correctly
        "SECRET_KEY": "supersecretkey",  # Ensure a secret key is set for session management
        "USE_COOKIES": True  # Explicitly enable cookies
    })

    # Create the application context and initialize the database
    with app.app_context():
        from app.models import db
        db.create_all()

    # Provide the test client for the application, ensuring cookies are used
    yield app.test_client(use_cookies=True)

    # Clean up the database after tests are done
    with app.app_context():
        db.session.remove()
        db.drop_all()
