# tests/test_news.py

import pytest
import responses
import logging
from app import create_app
from app.models import db

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Fixture for the application setup
@pytest.fixture
def app():
    app = create_app('test_config.Config')
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()
        db.drop_all()

# Fixture for the test client
@pytest.fixture
def client(app):
    return app.test_client()

@responses.activate
def test_get_security_news_success(client, capsys):
    logger.info("Starting test_get_security_news_success")

    # Mock the GET request to the News API
    responses.add(
        responses.GET,
        'https://newsapi.org/v2/everything?q=cybersecurity&sortBy=publishedAt&apiKey=9e0ce5f9adee4c6797f22ac1417e3889',
        json={
            "status": "ok",
            "totalResults": 1,
            "articles": [
                {
                    "source": {"id": None, "name": "Example"},
                    "author": "Author Name",
                    "title": "Example News Title",
                    "description": "Example News Description",
                    "url": "https://example.com/news",
                    "urlToImage": "https://example.com/image.jpg",
                    "publishedAt": "2023-06-08T12:00:00Z",
                    "content": "Example content"
                }
            ]
        },
        status=200
    )

    # Perform the test request
    response = client.get('/news/')
    assert response.status_code == 200
    response_data = response.get_json()

    logger.info("Response data: %s", response_data)
    print("Response JSON:", response_data)

    assert "articles" in response_data
    assert len(response_data["articles"]) == 1
    assert response_data["articles"][0]["title"] == "Example News Title"

    captured = capsys.readouterr()
    logger.info("Captured output: %s", captured.out)
    print("Captured output:", captured.out)

@responses.activate
def test_get_security_news_failure(client, capsys):
    logger.info("Starting test_get_security_news_failure")

    # Mock the GET request to the News API with a failure response
    responses.add(
        responses.GET,
        'https://newsapi.org/v2/everything?q=cybersecurity&sortBy=publishedAt&apiKey=9e0ce5f9adee4c6797f22ac1417e3889',
        json={"status": "error", "message": "Invalid API key"},
        status=401
    )

    # Perform the test request
    response = client.get('/news/')
    assert response.status_code == 500
    response_data = response.get_json()

    logger.info("Response data: %s", response_data)
    print("Response JSON:", response_data)

    assert "error" in response_data
    assert response_data["error"] == "Failed to fetch news"

    captured = capsys.readouterr()
    logger.info("Captured output: %s", captured.out)
    print("Captured output:", captured.out)

if __name__ == "__main__":
    pytest.main(["-v", "-s"])
