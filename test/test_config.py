# test_config.py

# Base configuration class for tests
class Config:
    TESTING = True  # Enable testing mode
    SECRET_KEY = 'test_secret_key'  # Set a secret key for sessions
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test-db.sqlite'  # Use a file-based SQLite database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Disable SQLAlchemy track modifications
    INVITATION_CODE = 'test'  # Set the valid invitation token
    # Add any other configuration needed for tests

# In-memory configuration class for tests
class InMemoryConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'  # Use an in-memory SQLite database
    INVITATION_CODE = 'test'  # Set the valid invitation token
