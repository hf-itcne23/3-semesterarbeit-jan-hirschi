# tests/test_url_scan.py

import pytest
import responses
from app import create_app
from app.models import db

# Fixture for the application setup
@pytest.fixture
def app():
    app = create_app('test_config.Config')
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()
        db.drop_all()

# Fixture for the test client
@pytest.fixture
def client(app):
    return app.test_client()

@responses.activate
def test_scan_url_malicious(client, capsys):
    # Mock the GET request to the URL being scanned
    responses.add(
        responses.GET,
        'http://test.com/',
        json={},
        status=200
    )

    # Mock the initial URL submission to VirusTotal
    responses.add(
        responses.POST,
        'https://www.virustotal.com/api/v3/urls',
        json={
            "data": {
                "id": "12345",
                "links": {
                    "self": "https://www.virustotal.com/api/v3/analyses/12345"
                }
            }
        },
        status=200
    )

    # Mock the analysis status check
    responses.add(
        responses.GET,
        'https://www.virustotal.com/api/v3/analyses/12345',
        json={
            "data": {
                "id": "12345",
                "attributes": {
                    "status": "completed",
                    "stats": {
                        "malicious": 1,
                        "suspicious": 0,
                        "harmless": 0
                    }
                }
            }
        },
        status=200
    )

    # Perform the test request
    response = client.post('/url_scan/', json={"url": "http://test.com"})
    assert response.status_code == 200
    assert response.json == {"result": "Malicious content found"}
    print(f"Response: {response.json}")

    captured = capsys.readouterr()
    print(captured.out)

@responses.activate
def test_scan_url_suspicious(client, capsys):
    # Mock the GET request to the URL being scanned
    responses.add(
        responses.GET,
        'http://test.com/',
        json={},
        status=200
    )

    # Mock the initial URL submission to VirusTotal
    responses.add(
        responses.POST,
        'https://www.virustotal.com/api/v3/urls',
        json={
            "data": {
                "id": "12345",
                "links": {
                    "self": "https://www.virustotal.com/api/v3/analyses/12345"
                }
            }
        },
        status=200
    )

    # Mock the analysis status check
    responses.add(
        responses.GET,
        'https://www.virustotal.com/api/v3/analyses/12345',
        json={
            "data": {
                "id": "12345",
                "attributes": {
                    "status": "completed",
                    "stats": {
                        "malicious": 0,
                        "suspicious": 1,
                        "harmless": 0
                    }
                }
            }
        },
        status=200
    )

    # Perform the test request
    response = client.post('/url_scan/', json={"url": "http://test.com"})
    assert response.status_code == 200
    assert response.json == {"result": "Suspicious content found"}
    print(f"Response: {response.json}")

    captured = capsys.readouterr()
    print(captured.out)

@responses.activate
def test_scan_url_harmless(client, capsys):
    # Mock the GET request to the URL being scanned
    responses.add(
        responses.GET,
        'http://test.com/',
        json={},
        status=200
    )

    # Mock the initial URL submission to VirusTotal
    responses.add(
        responses.POST,
        'https://www.virustotal.com/api/v3/urls',
        json={
            "data": {
                "id": "12345",
                "links": {
                    "self": "https://www.virustotal.com/api/v3/analyses/12345"
                }
            }
        },
        status=200
    )

    # Mock the analysis status check
    responses.add(
        responses.GET,
        'https://www.virustotal.com/api/v3/analyses/12345',
        json={
            "data": {
                "id": "12345",
                "attributes": {
                    "status": "completed",
                    "stats": {
                        "malicious": 0,
                        "suspicious": 0,
                        "harmless": 1
                    }
                }
            }
        },
        status=200
    )

    # Perform the test request
    response = client.post('/url_scan/', json={"url": "http://test.com"})
    assert response.status_code == 200
    assert response.json == {"result": "No malicious or suspicious content found"}
    print(f"Response: {response.json}")

    captured = capsys.readouterr()
    print(captured.out)

if __name__ == "__main__":
    pytest.main()
