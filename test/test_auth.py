# test/test_auth.py

import pytest
from flask import url_for
from app.models import db
from app.models.users import User
from app import create_app
import os

# Fixture for the application setup
@pytest.fixture
def app():
    os.environ['INVITATION_CODE'] = 'test'  # Ensure the invitation token environment variable is set
    app = create_app('test_config.InMemoryConfig')  # Use the in-memory configuration
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()
        db.drop_all()

# Fixture for the test client
@pytest.fixture
def client(app):
    return app.test_client()

# Test for user registration
def test_register_user(client, app, capsys):
    with app.app_context():
        with app.test_request_context():
            # Define the user data
            user_data = {
                'username': 'testuser',
                'email': 'testuser@example.com',
                'password1': 'password',
                'invitation_token': 'test'
            }

            print(f"Registering user with data: {user_data}")
            # Make a POST request to the user registration endpoint
            response = client.post(url_for('auth.register'), data=user_data, follow_redirects=True)

            # Print response data for debugging
            response_html = response.data.decode('utf-8')

            # Check if the user was created successfully
            assert response.status_code == 200, "User registration failed"
            print("User registration was successful.")

            # Verify the user exists in the database
            user = User.query.filter_by(email=user_data['email']).first()
            print(f"User found in database: {user}")
            assert user is not None, "User not found in the database"

            assert user.username == user_data['username'], "Username does not match"
            print(f"User {user.username} has the correct username.")

            # Capture and print the captured output
            captured = capsys.readouterr()
            print(captured.out)

# Test for user login
def test_login_user(client, app, capsys):
    with app.app_context():
        with app.test_request_context():
            # First, register a new user
            user_data = {
                'username': 'testuser',
                'email': 'testuser@example.com',
                'password1': 'password',
                'invitation_token': 'test'
            }

            client.post(url_for('auth.register'), data=user_data, follow_redirects=True)

            # Then, try to log in with the same credentials
            login_data = {
                'username': user_data['username'],
                'password': user_data['password1']
            }

            response = client.post(url_for('auth.login'), data=login_data, follow_redirects=True)

            # Print response data for debugging
            response_html = response.data.decode('utf-8')

            # Check if the login was successful
            assert response.status_code == 200, "User login failed"
            print("User login was successful.")

            captured = capsys.readouterr()
            print(captured.out)
