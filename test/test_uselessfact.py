# tests/test_uselessfact.py

import pytest
import responses
import logging
from app import create_app
from app.models import db
from test.test_config import Config

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Fixture for the application setup
@pytest.fixture
def app():
    app = create_app('test_config.Config')
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()
        db.drop_all()

# Fixture for the test client
@pytest.fixture
def client(app):
    return app.test_client()

@responses.activate
def test_get_useless_fact_success(client, capsys):
    logger.info("Starting test_get_useless_fact_success")

    # Mock the GET request to the Useless Facts API
    responses.add(
        responses.GET,
        'https://uselessfacts.jsph.pl/random.json?language=en',
        json={
            "id": "12345",
            "text": "This is a useless fact.",
            "source": "https://uselessfacts.jsph.pl"
        },
        status=200
    )

    # Perform the test request
    response = client.get('/uselessfact/')
    assert response.status_code == 200
    response_data = response.get_json()

    logger.info("Response data: %s", response_data)
    print("Response JSON:", response_data)

    assert "fact" in response_data
    assert response_data["fact"] == "This is a useless fact."

    captured = capsys.readouterr()
    logger.info("Captured output: %s", captured.out)
    print("Captured output:", captured.out)

@responses.activate
def test_get_useless_fact_failure(client, capsys):
    logger.info("Starting test_get_useless_fact_failure")

    # Mock the GET request to the Useless Facts API with a failure response
    responses.add(
        responses.GET,
        'https://uselessfacts.jsph.pl/random.json?language=en',
        json={"error": "Failed to fetch fact"},
        status=500
    )

    # Perform the test request
    response = client.get('/uselessfact/')
    assert response.status_code == 500
    response_data = response.get_json()

    logger.info("Response data: %s", response_data)
    print("Response JSON:", response_data)

    assert "error" in response_data
    assert response_data["error"] == "Failed to fetch fact"

    captured = capsys.readouterr()
    logger.info("Captured output: %s", captured.out)
    print("Captured output:", captured.out)

if __name__ == "__main__":
    pytest.main(["-v", "-s"])
