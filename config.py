import os  # Import the os module for interacting with the operating system

# Determine the base directory of the current file
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    # Secret key for the application, used for session management and security purposes
    SECRET_KEY = os.environ.get('SECRET_KEY')

    # URI for the database, retrieved from the environment variables
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI')

    # Disable SQLAlchemy event system to save resources, since it is not needed here
    SQLALCHEMY_TRACK_MODIFICATIONS = False
