# Use the official Python image from the Docker Hub
FROM python:3.11-slim

# Set environment variables
ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/app

# Create and set the working directory
WORKDIR /app

# Copy the requirements.txt into the working directory
COPY requirements.txt /app/

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code into the working directory
COPY . /app/

# Expose the port where the application will run
EXPOSE 8080

# Set environment variables for development
ENV INVITATION_CODE=test
ENV PHONE_VALIDATION_API_KEY=fdb34ec2d10833ca9906714752ce4d53

# Command to run on container start
CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:8080", "main:app"]
