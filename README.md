# Security Hirschi - API

## API Design

### HTTP Method

| Method | Route         | Params | Returns      | Effect                                                     |
| ------ | ------------- | ------ | ------------ | ---------------------------------------------------------- |
| POST   | /url_scan     | params | Status       | Scans a URL with the VirusTotal API                        |
| GET    | /uselessfact/ |        | Useless Fact | Calls the Uselessfact API to get a funny uselessfact       |
| GET    | /news/        |        | News         | Calls the Newsapi and to find Cybersecurity relevant news. |


![Abbild 8: Architektur](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/Architektur.png?ref_type=heads)

![Abbild 9: Database](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/Database.png?ref_type=heads)

## Ablauf

- **Kunde interagiert mit der Anwendung:**
    - Der Kunde greift über seinen Browser auf die Website zu.
    - Die Anfrage des Kunden wird an den Proxy-Container weitergeleitet.

- **Proxy leitet die Anfrage weiter:**
    - Der Proxy-Container empfängt die Anfrage des Kunden.
    - Der Proxy leitet die Anfrage an das Frontend der APIFlask-Anwendung weiter.

- **Frontend verarbeitet die Anfrage:**
    - Das APIFlask Frontend empfängt die Anfrage vom Proxy.
    - Das Frontend sendet die Anfrage zur weiteren Verarbeitung an das APIFlask Backend.

- **Backend verarbeitet Login-Anfragen:**
    - Das APIFlask Backend empfängt die Anfrage vom Frontend.
    - Bei einer Login-Anfrage sendet das Backend die Login-Daten an die Datenbank zur Überprüfung.

- **Datenbank validiert Login-Daten:**
    - Die Datenbank überprüft die Login-Daten (Benutzername, E-Mail und Passwort-Hash).
    - Die Datenbank antwortet mit dem Ergebnis der Überprüfung an das Backend.

- **Backend greift auf externe APIs zu:**
    - Nach der Validierung durch die Datenbank kann das Backend zusätzliche Daten von externen APIs anfordern.
    - Das Backend sendet Anfragen an verschiedene externe APIs (z.B. Useless Facts, ICT Cyber News, Phonenumber Validator und VirusTotal).
    - Die externen APIs antworten mit den angeforderten Daten.

- **Backend sendet Daten zurück an das Frontend:**
    - Das Backend empfängt die Daten von den externen APIs.
    - Das Backend sendet die verarbeiteten Daten zurück an das Frontend.

- **Frontend antwortet dem Kunden:**
    - Das Frontend empfängt die Daten vom Backend.
    - Das Frontend leitet die Daten an den Proxy weiter.

- **Proxy antwortet dem Kunden:**
    - Der Proxy empfängt die Daten vom Frontend.
    - Der Proxy sendet die Daten als Antwort zurück an den Kunden.

- **Bereitstellung des Codes:**
    - Der Code für die Anwendung wird in einem GitLab-Repository gespeichert.
    - Über eine Pipeline wird der Code vom GitLab-Repository auf die AWS EC2-Instanz gepusht.

## Database

- **Tabelle: Datenbank**
    - Die Datenbank enthält eine Tabelle, die für die Speicherung von Benutzerinformationen zuständig ist.

- **Spalten/Attribute:**
    - **Benutzername (`username`)**
        - Typ: Zeichenkette (String)
        - Beschreibung: Diese Spalte speichert den Benutzernamen jedes Benutzers.
    - **E-Mail (`mail`)**
        - Typ: Zeichenkette (String)
        - Beschreibung: Diese Spalte speichert die E-Mail-Adresse jedes Benutzers.
    - **Passwort-Hash (`password_hash`)**
        - Typ: Zeichenkette (String)
        - Beschreibung: Diese Spalte speichert den Hash-Wert des Passworts jedes Benutzers. Anstatt das tatsächliche Passwort zu speichern, wird ein verschlüsselter Hash gespeichert, um die Sicherheit zu erhöhen.
